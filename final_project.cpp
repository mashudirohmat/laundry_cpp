#include <iostream>
#include <time.h>

using namespace std;
/* 
* Project name : Wangi Laundry
* Member       : 1. Mashudi Rohmat -- 20.22.2421
*                2. Gustu Maulana Firmansyah -- 20.22.2415
*                3. Karina Novita Widyanti -- 20.22.2389
*                4. Siti Maulidiyah Sari -- 20.22.2387
* Years        : 2020
*/
struct laundry {
	string name;
	string queue_number;
	int weight;
	int day;
};

laundry laund[20];

int printTime(){
	time_t my_time = time(NULL); 
    printf("%s", ctime(&my_time)); 
    return 0;
}

int count_price(int weight,int day){
	int price=0;
	if(day==1)price = weight*10000;
	else if(day==2) price = weight*5000;
	else if(day==3 || day>3) price = weight*3000;
	
	return price;
}

void add_laundry(){
	int max = sizeof(laund)/sizeof(laund[0]);
	int index = 0;
	for (int i=0; i<max;i++){
		if (laund[i].name == ""){
			index = i;
		}
		break;
	}
	cout<<index;
	cout<<"Enter customer data"<<endl;
	cout<<"==========================="<<endl;
	cout<<"1. Customer name : ";
	cin>>laund[index].name;
	cout<<"2. Weight        : ";
	cin>>laund[index].weight;
	cout<<"3. Day           : ";
	cin>>laund[index].day;
}

void list_laundry(){
	int max = sizeof(laund)/sizeof(laund[0]);
	int index = 0;
	for (int i=0; i<max;i++){
		if (laund[i].name == ""){
			index = i;
		}
		break;
	}
	cout<<index;
	cout<<"***********Laundry List***********"<<endl;
	cout<<"=================================="<<endl;
	cout<<endl;
	cout<<"||         Name           ||           Weight          ||           Day"<<endl;
	for (int x=0;x < index;x++){
		cout<<"||      ";
		cout<<laund[x].name;
		cout<<"      ||      ";
		cout<<laund[x].weight;
		cout<<"      ||      ";
		cout<<laund[x].day;
		cout<<endl;
	}
}

int my_application() {
   	printTime();
   	
	int main_option;
	cout<<"Wangi Laundry"<<endl;
	cout<<"==========================="<<endl;
	cout<<"1. Add laundry list"<<endl;
	cout<<"2. List Laundry"<<endl;
	cout<<"Choose menu : ";
	cin>>main_option;
	
	if(main_option == 1){
		add_laundry();
		my_application();
	}else if (main_option == 2){
		list_laundry();
	}else{
		cout<<"Your choice isn't correct!'";
	}
	return 0;
}


int main() {
   return my_application();
}
